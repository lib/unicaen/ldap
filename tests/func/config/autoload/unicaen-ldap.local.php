<?php

return array(
   'unicaen-ldap' => array(
       'host'                => 'ldev.unicaen.fr',
       'port'                => 389,
       'version'             => 3,
       'baseDn'              => "dc=unicaen,dc=fr",
       'bindRequiresDn'      => true,
       'username'            => "uid=applidev,ou=system,dc=unicaen,dc=fr",
       'password'            => "xxxxxxxxxxxxxxx",
       'accountFilterFormat' => "(&(objectClass=posixAccount)(supannAliasLogin=%s))",
       'useStartTls'         => false
   )
);