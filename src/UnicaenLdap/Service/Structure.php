<?php

namespace UnicaenLdap\Service;

use UnicaenLdap\Entity\Entity;
use UnicaenLdap\Entity\Structure as StructureEntity;

/**
 * Classe regroupant les opérations de recherche de structures dans l'annuaire LDAP.
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class Structure extends AbstractService
{
    /**
     * Type de l'entité
     *
     * @var string
     */
    protected $type = 'Structure';

    /**
     * Organizational Units
     *
     * @var array
     */
    protected $ou = [
        'structures'
    ];

    /**
     * Préfixe utilisé pour le code des structures
     *
     * @var string
     */
    protected $code_structure_prefixe = 'HS_';

    /**
     * Préfixe utilisé pour le code des modules d'enseignement
     *
     * @var string
     */
    protected $code_module_enseignement_prefixe = 'AE_';

    /**
     * Code de la structure lié à l'établissement
     *
     * @var string
     */
    protected $code_structure_mere = 'UNIV';


    /**
     * Retourne le préfixe utilisé pour les codes des structures
     *
     * @return string
     */
    public function getCodeStructurePrefixe()
    {
        return $this->code_structure_prefixe;
    }

    /**
     * Retourne le préfixe utilisé pour les codes des modules d'enseignement
     *
     * @return string
     */
    public function getCodeModuleEnseignementPrefixe()
    {
        return $this->code_module_enseignement_prefixe;
    }

    /**
     * Retourne la structure mère : Université
     *
     * @return StructureEntity
     */
    public function getStructureMere()
    {
        return $this->get(sprintf('%s%s', $this->code_structure_prefixe, $this->code_structure_mere));
    }
}