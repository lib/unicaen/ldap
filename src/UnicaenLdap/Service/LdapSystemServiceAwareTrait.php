<?php

namespace UnicaenLdap\Service;

use UnicaenLdap\Service\System as LdapSystemService;

trait LdapSystemServiceAwareTrait
{
    /**
     * @var LdapSystemService
     */
    protected $ldapSystemService;

    /**
     * @param LdapSystemService $ldapSystemService
     */
    public function setLdapSystemService(LdapSystemService $ldapSystemService)
    {
        $this->ldapSystemService = $ldapSystemService;
    }

    /**
     * @return LdapSystemService
     */
    public function getLdapSystemService()
    {
        return $this->ldapSystemService;
    }
}