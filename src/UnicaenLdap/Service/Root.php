<?php

namespace UnicaenLdap\Service;

use UnicaenLdap\Entity\Entity;
use UnicaenLdap\Entity\Root as RootEntity;
use UnicaenLdap\Entity\Structure as StructureEntity;
use UnicaenLdap\Ldap;

/**
 * Classe de service pour la racine l'annuaire LDAP.
 *
 * @author David Surville <david.surville@unicaen.fr>
 */
class Root extends AbstractService
{
    /**
     * Type de l'entité
     *
     * @var string
     */
    protected $type = 'Root';

    /**
     * Distinguished Name de l'établissement
     *
     * @var string
     */
    protected $etablissement_dn = 'dc=unicaen,dc=fr';

    /**
     * Domaine de l'établissement
     *
     * @var string
     */
    protected $etablissement_domain = 'unicaen.fr';

    /**
     * Code UAI de l'établissement
     *
     * @var string
     */
    protected $etablissement_uai = '0141408E';

    /**
     * Retourne le type du service
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getEtablissementDN()
    {
        return $this->etablissement_dn;
    }

    /**
     * @return string
     */
    public function getEtablissementDomain()
    {
        return $this->etablissement_domain;
    }

    /**
     * @return string
     */
    public function getEtablissementUAI()
    {
        return $this->etablissement_uai;
    }

    /**
     * Retourne l'entité racine
     *
     * @return RootEntity
     */
    public function getEntity()
    {
        list($key) = Entity::getNodeParams($this->type);
        return $this->getBy($this->etablissement_dn, 'dn');
    }

    /**
     * Retourne l'entité dans la branche "structures" liée à l'établissement
     *
     * @return StructureEntity
     */
    public function getStructureEntity()
    {
        return $this->getLdapStructureService()->getStructureMere();
    }
}