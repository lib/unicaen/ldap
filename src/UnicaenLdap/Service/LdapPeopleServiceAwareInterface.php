<?php

namespace UnicaenLdap\Service;

use UnicaenLdap\Service\People as LdapPeopleService;

interface LdapPeopleServiceAwareInterface
{
    /**
     * @param LdapPeopleService $ldapPeopleService
     * @return mixed
     */
    public function setLdapPeopleService(LdapPeopleService $ldapPeopleService);

    /**
     * @return LdapPeopleService
     */
    public function getLdapPeopleService();
}