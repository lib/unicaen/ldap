<?php

namespace UnicaenLdap\Service;

use DateTime;
use UnicaenLdap\Collection;

/**
 * Classe regroupant les opérations de recherche de groupes dans l'annuaire LDAP.
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class Group extends AbstractService
{
    /**
     * Type de l'entité
     *
     * @var string
     */
    protected $type = 'Group';

    /**
     * Organizational Units
     *
     * @var array
     */
    protected $ou = [
        'groups'
    ];

    /**
     * Filtre une liste de groupes pour ne retourner que ceux qui sont encore valides
     *
     * @param array|Collection $groups
     * @return array
     */
    public function filterValids( $groups, DateTime $dateObservation = null )
    {
        if (empty($dateObservation)) $dateObservation = new DateTime;

        $result = array();
        foreach( $groups as $id => $group ){
            if ($group->isValid($dateObservation)) $result[$id] = $group;
        }

        return $result;
    }
}