<?php

namespace UnicaenLdap\Service;

use UnicaenLdap\Service\Structure as LdapStructureService;

interface LdapStructureServiceAwareInterface
{
    /**
     * @param LdapStructureService $ldapStructureService
     * @return mixed
     */
    public function setLdapStructureService(LdapStructureService $ldapStructureService);

    /**
     * @return LdapStructureService
     */
    public function getLdapStructureService();
}