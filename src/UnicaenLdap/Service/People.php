<?php

namespace UnicaenLdap\Service;

/**
 * Classe regroupant les opérations de recherche de personnes dans l'annuaire LDAP.
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class People extends AbstractService
{
    /**
     * Type de l'entité
     *
     * @var string
     */
    protected $type = 'People';

    /**
     * Organizational Units
     *
     * @var array
     */
    protected $ou = [
        'people',
        'deactivated',
        'blocked'
    ];

    public function checkPassword($value)
    {
        /**
         * @todo implémenter la fonction de vérification d'un mot de passe
         */
    }
}