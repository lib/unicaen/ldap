<?php

namespace UnicaenLdap\Service;

use Interop\Container\ContainerInterface;
use UnicaenLdap\Ldap;
use UnicaenLdap\Service;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 *
 *
 * @author Unicaen
 */
class SystemFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    /**
     * Create service
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return object|System
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var Ldap $ldap
         * @var Service\Group $ldapGroupService
         * @var Service\Generic $ldapGenericService
         * @var Service\People $ldapPeopleService
         * @var Service\Root $ldapRootService
         * @var Service\Structure $ldapStructureService
         */
        $ldap                   = $container->get('ldap');
        $ldapGenericService     = $container->get('ldapServiceGeneric');
        $ldapGroupService       = $container->get('ldapServiceGroup');
        $ldapPeopleService      = $container->get('ldapServicePeople');
        $ldapRootService        = $container->get('ldapServiceRoot');
        $ldapStructureService   = $container->get('ldapServiceStructure');

        $service = new System();
        $service->setLdap($ldap);
        $service->setLdapPeopleService($ldapPeopleService);
        $service->setLdapStructureService($ldapStructureService);
        $service->setLdapGroupService($ldapGroupService);
        $service->setLdapRootService($ldapRootService);
        $service->setLdapGenericService($ldapGenericService);

        return $service;
    }
}
