<?php

namespace UnicaenLdap\Service;

use UnicaenLdap\Service\Structure as LdapStructureService;

trait LdapStructureServiceAwareTrait
{
    /**
     * @var LdapStructureService
     */
    protected $ldapStructureService;

    /**
     * @param LdapStructureService $ldapStructureService
     */
    public function setLdapStructureService(LdapStructureService $ldapStructureService)
    {
        $this->ldapStructureService = $ldapStructureService;
    }

    /**
     * @return LdapStructureService
     */
    public function getLdapStructureService()
    {
        return $this->ldapStructureService;
    }
}