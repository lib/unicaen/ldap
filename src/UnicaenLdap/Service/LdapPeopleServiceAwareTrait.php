<?php

namespace UnicaenLdap\Service;

use UnicaenLdap\Service\People as LdapPeopleService;

trait LdapPeopleServiceAwareTrait
{
    /**
     * @var LdapPeopleService
     */
    protected $ldapPeopleService;

    /**
     * @param LdapPeopleService $ldapPeopleService
     */
    public function setLdapPeopleService(LdapPeopleService $ldapPeopleService)
    {
        $this->ldapPeopleService = $ldapPeopleService;
    }

    /**
     * @return LdapPeopleService
     */
    public function getLdapPeopleService()
    {
        return $this->ldapPeopleService;
    }
}