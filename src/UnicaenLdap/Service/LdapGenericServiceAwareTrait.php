<?php

namespace UnicaenLdap\Service;

use UnicaenLdap\Service\Generic as LdapGenericService;

trait LdapGenericServiceAwareTrait
{
    /**
     * @var LdapGenericService
     */
    protected $ldapGenericService;

    /**
     * @param LdapGenericService $ldapGenericService
     */
    public function setLdapGenericService(LdapGenericService $ldapGenericService)
    {
        $this->ldapGenericService = $ldapGenericService;
    }

    /**
     * @return LdapGenericService
     */
    public function getLdapGenericService()
    {
        return $this->ldapGenericService;
    }
}