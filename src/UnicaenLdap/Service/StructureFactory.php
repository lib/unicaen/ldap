<?php

namespace UnicaenLdap\Service;

use Interop\Container\ContainerInterface;
use UnicaenLdap\Ldap;
use UnicaenLdap\Service;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 *
 *
 * @author Unicaen
 */
class StructureFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    /**
     * Create service
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return object|Structure
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var Ldap $ldap
         * @var Service\Generic $ldapGenericService
         * @var Service\Group $ldapGroupService
         * @var Service\People $ldapPeopleService
         * @var Service\Root $ldapRootService
         * @var Service\System $ldapSystemService
         */
        $ldap                   = $container->get('ldap');
        $ldapGenericService     = $container->get('ldapServiceGeneric');
        $ldapGroupService       = $container->get('ldapServiceGroup');
        $ldapPeopleService      = $container->get('ldapServicePeople');
        $ldapRootService        = $container->get('ldapServiceRoot');
        $ldapSystemService      = $container->get('ldapServiceSystem');

        $service = new Structure();
        $service->setLdap($ldap);
        $service->setLdapGenericService($ldapGenericService);
        $service->setLdapGroupService($ldapGroupService);
        $service->setLdapPeopleService($ldapPeopleService);
        $service->setLdapRootService($ldapRootService);
        $service->setLdapSystemService($ldapSystemService);

        return $service;
    }
}
