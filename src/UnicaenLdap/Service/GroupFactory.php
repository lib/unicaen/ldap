<?php

namespace UnicaenLdap\Service;

use Interop\Container\ContainerInterface;
use UnicaenLdap\Ldap;
use UnicaenLdap\Service;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

class GroupFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    /**
     * Create service
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return object|Group
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var Ldap $ldap
         * @var Service\Generic $ldapGenericService
         * @var Service\People $ldapPeopleService
         * @var Service\Root $ldapRootService
         * @var Service\Structure $ldapStructureService
         * @var Service\System $ldapSystemService
         */
        $ldap                   = $container->get('ldap');
        $ldapGenericService     = $container->get('ldapServiceGeneric');
        $ldapPeopleService      = $container->get('ldapServicePeople');
        $ldapRootService        = $container->get('ldapServiceRoot');
        $ldapStructureService   = $container->get('ldapServiceStructure');
        $ldapSystemService      = $container->get('ldapServiceSystem');

        $service = new Group();
        $service->setLdap($ldap);
        $service->setLdapGenericService($ldapGenericService);
        $service->setLdapPeopleService($ldapPeopleService);
        $service->setLdapRootService($ldapRootService);
        $service->setLdapStructureService($ldapStructureService);
        $service->setLdapSystemService($ldapSystemService);

        return $service;
    }
}
