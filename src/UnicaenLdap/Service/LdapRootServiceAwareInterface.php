<?php

namespace UnicaenLdap\Service;

use UnicaenLdap\Service\Root as LdapRootService;

interface LdapRootServiceAwareInterface
{
    /**
     * @param LdapRootService $ldapRootService
     * @return mixed
     */
    public function setLdapRootService(LdapRootService $ldapRootService);

    /**
     * @return LdapRootService
     */
    public function getLdapRootService();
}