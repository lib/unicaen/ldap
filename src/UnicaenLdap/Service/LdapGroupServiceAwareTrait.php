<?php

namespace UnicaenLdap\Service;

use UnicaenLdap\Service\Group as LdapGroupService;

trait LdapGroupServiceAwareTrait
{
    /**
     * @var LdapGroupService
     */
    protected $ldapGroupService;

    /**
     * @param LdapGroupService $ldapGroupService
     */
    public function setLdapGroupService(LdapGroupService $ldapGroupService)
    {
        $this->ldapGroupService = $ldapGroupService;
    }

    /**
     * @return LdapGroupService
     */
    public function getLdapGroupService()
    {
        return $this->ldapGroupService;
    }
}