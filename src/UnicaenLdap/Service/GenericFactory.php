<?php

namespace UnicaenLdap\Service;

use Interop\Container\ContainerInterface;
use UnicaenLdap\Ldap;
use UnicaenLdap\Service;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

class GenericFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    /**
     * Create service
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return object|Generic
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var Ldap $ldap
         * @var Service\Group $ldapGroupService
         * @var Service\People $ldapPeopleService
         * @var Service\Root $ldapRootService
         * @var Service\Structure $ldapStructureService
         * @var Service\System $ldapSystemService
         */
        $ldap                   = $container->get('ldap');
        $ldapGroupService       = $container->get('ldapServiceGroup');
        $ldapPeopleService      = $container->get('ldapServicePeople');
        $ldapRootService        = $container->get('ldapServiceRoot');
        $ldapStructureService   = $container->get('ldapServiceStructure');
        $ldapSystemService      = $container->get('ldapServiceSystem');

        $service = new Generic();
        $service->setLdap($ldap);
        $service->setLdapGroupService($ldapGroupService);
        $service->setLdapPeopleService($ldapPeopleService);
        $service->setLdapRootService($ldapRootService);
        $service->setLdapStructureService($ldapStructureService);
        $service->setLdapSystemService($ldapSystemService);

        return $service;
    }
}
