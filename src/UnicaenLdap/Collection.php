<?php


namespace UnicaenLdap;

use Countable;
use Iterator;
use UnicaenLdap\Service\AbstractService;
use UnicaenLdap\Entity\Entity;

/**
 * Liste d'entités
 */
class Collection implements Iterator, Countable
{
    /**
     * @var AbstractService
     */
    private AbstractService $service;

    /**
     * Liste des identifiants à parcourir
     *
     * @var string[]
     */
    private array $data;

    /**
     * @var integer
     */
    private int $index = 0;


    /**
     * Constructor.
     *
     * @param AbstractService $service
     * @param string[]        $data
     */
    public function __construct(AbstractService $service, array $data)
    {
        $this->service = $service;
        $this->data = $data;
        $this->index = 0;
    }

    /**
     * Returns the number of items in current result
     * Implements Countable
     *
     * @return int
     */
    public function count(): int
    {
        return count($this->data);
    }

    /**
     * Return the current result item
     * Implements Iterator
     *
     * @return Entity
     * @throws \UnicaenLdap\Exception
     */
    public function current(): mixed
    {
        $current = $this->data[$this->index];

        return $this->service->get($current);
    }

    /**
     * Return the current result item key
     * Implements Iterator
     *
     * @return int|null
     */
    public function key(): mixed
    {
        return $this->index;
    }

    /**
     * Move forward to next result item
     * Implements Iterator
     *
     */
    public function next(): void
    {
        ++$this->index;
    }

    /**
     * Rewind the Iterator to the first result item
     * Implements Iterator
     */
    public function rewind(): void
    {
        $this->index = 0;
    }

    /**
     * Check if there is a current result item
     * after calls to rewind() or next()
     * Implements Iterator
     *
     * @return bool
     */
    public function valid(): bool
    {
        return isset($this->data[$this->index]);
    }
}
