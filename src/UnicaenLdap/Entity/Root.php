<?php

namespace UnicaenLdap\Entity;

use UnicaenLdap\Entity\Base\Root as BaseRoot;
use UnicaenLdap\Filter\Filter;
use Laminas\Ldap\Exception\LdapException;

/**
 * Classe de gestion de l'entité racine de l'annuaire LDAP.
 *
 * @author David SURVILLE <david.surville@unicaen.fr>
 */
class Root extends BaseRoot
{}