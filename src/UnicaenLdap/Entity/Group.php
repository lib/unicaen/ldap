<?php

namespace UnicaenLdap\Entity;

use DateTime;
use UnicaenLdap\Entity\Base\Group as BaseGroup;

/**
 * Classe de gestion des entités de la branche "groups" de l'annuaire LDAP.
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class Group extends BaseGroup
{
    /**
     * Détermine si un groupe est valide ou non, c'est-à-dire si sa date de fin de validité est postérieure ou égal à la
     * date testée
     *
     * @param DateTime $dateObservation
     * @return bool
     */
    public function isValid(DateTime $dateObservation = null)
    {
        if (empty($dateObservation)) $dateObservation = new DateTime;
        $dateControle = $this->supannGroupeDateFin;

        return empty($dateControle) || ($dateControle >= $dateObservation->getTimestamp());
    }

    /**
     * Détermine si un groupe est obsolète ou non, c'est-à-dire si sa date de fin de validité est antérieure à la date
     * testée
     *
     * @param DateTime $dateObservation
     * @return bool
     */
    public function isObsolete(DateTime $dateObservation = null)
    {
        return !$this->isValid();
    }

    /**
     * Retourne la liste des personnes membres du groupe
     *
     * @param string $orderBy Champ de tri (au besoin)
     * @return People[]
     * @throws \Laminas\Ldap\Exception\LdapException
     */
    public function getMembres($orderBy = null)
    {
        $peopleService = $this->service->getLdapPeopleService();
        $result = $peopleService->getAllBy($this->member, 'dn', $orderBy);

        return $result;
    }
}