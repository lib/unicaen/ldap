<?php

namespace UnicaenLdap\Entity\Base;

use UnicaenLdap\Entity\Entity;
use UnicaenLdap\Exception;
use Laminas\Ldap\Attribute;
use Laminas\Ldap\Exception\LdapException;

/**
 * Classe mère des entités de la branche "generic" de l'annuaire LDAP.
 *
 * @author David Surville <david.surville@unicaen.fr>
 */
class Generic extends Entity
{
    /**
     * @var string
     */
    protected $type = 'Generic';

    /**
     * Liste des classes d'objet nécessaires à la création d'une adresse générique
     *
     * @var array
     */
    protected $objectClass = [
        'top',
        'inetOrgPerson',
        'organizationalPerson',
        'person',
    ];

    /**
     * Liste des attributs autorisés pour une entité "Generic"
     *
     * @var array
     */
    protected $authorizedAttributes = [
        // Attributes classes
        'objectClass',
        // Attributes
        'cn',
        'description',
        'mail',
        'sn',
        'supannAliasLogin',
        'ucbnServiceIMAP',
        'userPassword',
    ];


    /**
     * Liste des attributs contenant des dates
     *
     * @var string[]
     */
    protected $dateTimeAttributes = [
    ];

    /**
     * Liste des attributs monovalués
     *
     * @var array
     */
    protected $monoValuedAttributes = [
        'supannALiasLogin',
        'ucbnServiceIMAP',
        'userPassword',
    ];


    /**
     * Attribut Ldap "cn"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     */
    public function setCn($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $this->appendOrNot('cn', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "description"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     */
    public function setDescription($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $this->appendOrNot('description', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "mail"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setMail($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter(filter_var_array($value, FILTER_VALIDATE_EMAIL));
        $this->appendOrNot('mail', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "sn"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     */
    public function setSn($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $this->appendOrNot('sn', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannALiasLogin"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannAliasLogin($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map('strtolower', $value);
        $value = array_filter($value, function ($v) {
            return preg_match('/^[0-9a-z\-]+$/', $v);
        });

        $this->appendOrNot('supannAliasLogin', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "ucbnServiceIMAP"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setUcbnServiceIMAP($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map([$this, 'formatBoolean'], $value);
        $this->appendOrNot('ucbnServiceIMAP', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "userPassword"
     *
     * @param string $value
     * @return self
     * @throws LdapException
     */
    public function setUserPassword(string $value)
    {
        $this->getNode()->setPasswordAttribute($value, Attribute::PASSWORD_HASH_SHA, 'userPassword');

        return $this;
    }
}