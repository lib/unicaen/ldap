<?php

namespace UnicaenLdap\Entity;

use DateTime;
use UnicaenLdap\Entity\Base\People as BasePeople;
use UnicaenLdap\Entity\Group as GroupEntity;
use UnicaenLdap\Entity\Structure as StructureEntity;
use UnicaenLdap\Entity\System as SystemEntity;
use UnicaenLdap\Exception;
use Laminas\Ldap\Dn;

/**
 * Classe de gestion des entités de la branche "people" de l'annuaire LDAP.
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 * @author David SURVILLE <david.surville@unicaen.fr>
 */
class People extends BasePeople
{
    /**
     * Détermine si l'individu est actif ou non
     *
     * @return boolean
     */
    public function isPeople()
    {
        return 'people' == $this->getOu();
    }

    /**
     * Détermine si l'individu est désactivé ou non
     *
     * @return boolean
     */
    public function isDeactivated()
    {
        return 'deactivated' == $this->getOu();
    }

    /**
     * Détermine si l'individu est bloqué ou non
     *
     * @return boolean
     */
    public function isBlocked()
    {
        return 'blocked' == $this->getOu();
    }

    /**
     * Retourne le nom complet.
     *
     * @param boolean $uppercaseName nom de famille en majuscules
     * @param boolean $withCivility inclure la civilité
     * @param boolean $firstNameFirst prénom avant le nom de famille
     * @return string
     */
    public function getNomComplet($uppercaseName = false, $withCivility = false, $firstNameFirst = false)
    {
        $sn = $this->sn;
        $sn = is_array($sn) ? current($sn) : $sn;

        if (!$sn) {
            return '';
        }

        $nom = $uppercaseName ? mb_strtoupper($sn, 'UTF-8') : $sn;
        $prenom = $this->givenName;
        $civilite = $withCivility ? $this->supannCivilite . ' ' : '';

        return  $civilite . ($firstNameFirst ? $prenom . ' ' . $nom : $nom . ' ' . $prenom);
    }

    /**
     * Retourne le nom d'usage
     *
     * @return string|null
     */
    public function getNomUsage()
    {
        return is_array($this->sn) ? $this->sn[0] : $this->sn;
    }

    /**
     * Retourne le nom de famille s'il existe, sinon le nom d'usage
     *
     * @return string|null
     */
    public function getNomFamille()
    {
        return is_array($this->sn) ? $this->sn[1] : $this->sn;
    }

    /**
     * Retourne la date de naissance
     *
     * @return DateTime|false
     */
    public function getDateNaissance()
    {
        return DateTime::createFromFormat('Ymd h:i:s', $this->schacDateOfBirth . ' 00:00:00');
    }

    /**
     * Retourne la date d'expiration
     *
     * @return DateTime|false
     */
    public function getDateExpiration()
    {
        return DateTime::createFromFormat('Ymd h:i:s', $this->schacExpiryDate . ' 00:00:00');
    }

    /**
     * Retourne le responsable de l'entrée Ldap
     *
     * @return People|SystemEntity|string|null
     * @throws Exception
     * @throws \Laminas\Ldap\Exception\LdapException
     */
    public function getParrain()
    {
        $systemService = $this->service->getLdapSystemService();
        $node = $this->service->getLdap()->getNode($this->supannParrainDN);
        $branch = $node->getDn()->get(1);
        switch($branch['ou']) {
            case strtolower($this->service->getType()):
                return $this->service->getBy($node->getDnString(), 'dn');
            case strtolower($systemService->getType()):
                return $systemService->getBy($node->getDnString(), 'dn');
            default:
                return $this->supannParrainDN;
        }
    }

    /**
     * Retourne l'adresse postale professionnelle
     *
     * @param string $type type de l'adresse : 'professionnelle' ou 'personnelle'
     * @return array|null
     */
    public function getAdressePostale($type = 'professionnelle')
    {
        if(!in_array($type, ['professionnelle', 'personnelle'])) {
            return null;
        }

        $attributes = ($type == 'professionnelle')
            ? $this->postalAddress
            : [$this->ucbnPrivateAddress, $this->ucbnPrivateAddressBis];
        $value = $this->preFormat($attributes);
        $value = array_map(function ($val) {
            return explode('$', $val);
        }, $value);

        return 1 == count($value) ? $value[0] : $value;
    }

    /**
     * Test si une valeur est renseignée pour le champ "eduPersonAffiliation"
     *
     * @param string $status
     * @return bool
     */
    protected function whichStatus(string $status): bool
    {
        $value = $this->preFormat($this->eduPersonAffiliation);
        return in_array($status, $value);
    }

    /**
     * Check "affiliate" status
     *
     * @return bool
     */
    public function isAffiliate(): bool
    {
        return $this->whichStatus(parent::STATUS_AFFILIATE);
    }
    /**
     * Check "alum" status
     *
     * @return bool
     */
    public function isAlum(): bool
    {
        return $this->whichStatus(parent::STATUS_ALUM);
    }

    /**
     * Check "emeritus" status
     *
     * @return bool
     */
    public function isEmeritus(): bool
    {
        return $this->whichStatus(parent::STATUS_EMERITUS);
    }

    /**
     * Check "employee" status
     *
     * @return bool
     */
    public function isEmployee(): bool
    {
        return $this->whichStatus(parent::STATUS_EMPLOYEE);
    }

    /**
     * Check "faculty" status
     *
     * @return bool
     */
    public function isFaculty(): bool
    {
        return $this->whichStatus(parent::STATUS_FACULTY);
    }

    /**
     * Check "member" status
     *
     * @return bool
     */
    public function isMember(): bool
    {
        return $this->whichStatus(parent::STATUS_MEMBER);
    }

    /**
     * Check "registered reader" status
     *
     * @return bool
     */
    public function isRegisteredReader(): bool
    {
        return $this->whichStatus(parent::STATUS_REGISTERED_READER);
    }

    /**
     * Check "researcher" status
     *
     * @return bool
     */
    public function isResearcher(): bool
    {
        return $this->whichStatus(parent::STATUS_RESEARCHER);
    }

    /**
     * Check "retired" status
     *
     * @return bool
     */
    public function isRetired(): bool
    {
        return $this->whichStatus(parent::STATUS_RETIRED);
    }

    /**
     * Check "staff" status
     *
     * @return bool
     */
    public function isStaff(): bool
    {
        return $this->whichStatus(parent::STATUS_STAFF);
    }

    /**
     * Check "student" status
     *
     * @return bool
     */
    public function isStudent(): bool
    {
        return $this->whichStatus(parent::STATUS_STUDENT);
    }

    /**
     * Check "teacher" status
     *
     * @return bool
     */
    public function isTeacher(): bool
    {
        return $this->whichStatus(parent::STATUS_TEACHER);
    }

    /**
     * Retourne le léocode associé à l'individu
     *
     * @return string|null
     */
    public function getLeocarteCode()
    {
        $attributeValues = $this->preFormat($this->supannRefId);
        $label = $this->getLabel('LEOCODE');

        $value = array_filter($attributeValues, function ($v) use ($label) {
            return preg_match("/^$label(?<identifiant>.+)$/", $v);
        });

        return !empty($value)
            ? str_replace($label, '', array_values($value)[0])
            : null;
    }

    /**
     * Retourne le code de la léocarte associée à l'individu
     *
     * @return string|null
     */
    public function getLeocarteCSN()
    {
        $attributeValues = $this->preFormat($this->supannRefId);
        $label = $this->getLabel('UNICAEN', 'REVERSECSN');

        $value = array_filter($attributeValues, function ($v) use ($label) {
            return preg_match("/^$label(?<identifiant>.+)$/", $v);
        });

        return !empty($value)
            ? str_replace($label, '', array_values($value)[0])
            : null;
    }

    /**
     * Retourne le numéro d'étudiant
     *
     * @return string|null
     */
    public function getEtudiantId()
    {
        return $this->supannEtuId;
    }

    /**
     * Retourne le numéro d'employé
     *
     * @return string|null
     */
    public function getEmployeId()
    {
        return $this->supannEmpId;
    }

    /**
     * Retourne l'identifiant de l'individu dans le référentiel
     *
     * @return string|null
     */
    public function getReferentielId()
    {
        $attributeValues = $this->preFormat($this->supannRefId);
        $label = $this->getLabel('OCTOPUS', 'ID');

        $value = array_filter($attributeValues, function ($v) use ($label) {
            return preg_match("/^$label(?<identifiant>.+)$/", $v);
        });

        return !empty($value)
            ? str_replace($label, '', array_values($value)[0])
            : null;
    }

    /**
     * Établissement auquel appartient la personne
     *
     * @return StructureEntity|null
     */
    public function getEtablissement()
    {
        $rootService = $this->service->getLdapRootService();
        $structureService = $this->service->getLdapStructureService();
        $dn = $this->eduPersonOrgDN;

        if (empty($dn)) return null;
        switch($dn) {
            case $rootService->getEtablissementDN():
                return $rootService->getStructureEntity();
            default:
                try {
                    return $structureService->getBy($dn, 'dn');
                } catch (Exception $e) {
                    return null;
                }
        }
    }

    /**
     * Retourne les structures auxquelles appartient la personne
     *
     * @return StructureEntity|StructureEntity[]|null
     */
    public function getStructure()
    {
        $structureService = $this->service->getLdapStructureService();
        $value = $this->eduPersonOrgUnitDN;
        if (empty($value)) return null;

        $value = $structureService->getAllBy($value, 'dn');
        return 1 == count($value) ? array_shift($value) : $value;
    }

    /**
     * Retourne la structure principale à laquelle appartient la personne
     *
     * @return StructureEntity|null
     */
    public function getStructurePrincipale()
    {
        $structureService = $this->service->getLdapStructureService();
        $value = $this->eduPersonPrimaryOrgUnitDN;
        if (empty($value)) return null;

        try {
            return $structureService->getBy($value, 'dn');
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Retourne les structures recherche auxquelles appartient la personne
     *
     * @return StructureEntity|StructureEntity[]|null
     */
    public function getStructureRecherche()
    {
        $structureService = $this->service->getLdapStructureService();
        $structurePrefixe = $structureService->getCodeStructurePrefixe();

        $value = $this->preFormat($this->ucbnStructureRecherche);
        if (empty($value)) return null;

        $value = array_map(function ($v) use ($structurePrefixe) {
            preg_match(self::$structure_pattern, $v, $matches);
            return $structurePrefixe . $matches['code'];
        }, $value);

        $value = $structureService->getAllBy($value, 'supannCodeEntite');
        return 1 == count($value) ? array_shift($value) : $value;
    }

    /**
     * Vérifie l'appartenance d'une personne à une structure (structures recherche incluses)
     *
     * @param string|Dn|StructureEntity $value
     * @return bool
     * @throws Exception
     * @throws \Laminas\Ldap\Exception\LdapException
     */
    public function isInStructure($value)
    {
        $structureService = $this->service->getLdapStructureService();
        $structurePrefixe = $structureService->getCodeStructurePrefixe();

        if (is_string($value)) {
            if(Dn::checkDn($value)) {
                $value = $structureService->getBy($value, 'dn');
            }
            else {
                if (0 !== strpos($value, $structurePrefixe)) {
                    $value = $structurePrefixe . $value;
                }
                $value = $structureService->get($value);
            }
        } elseif ($value instanceof Dn) {
            $value = $structureService->getBy($value->toString(), 'dn');
        }

        if(!$value instanceof StructureEntity) {
            return false;
        }

        $structures = $this->preFormat($this->supannEntiteAffectation);
        $structuresRecherche = $this->preFormat($this->ucbnStructureRecherche);

        $structuresRecherche = array_map(function ($v) use ($structurePrefixe) {
            preg_match(self::$structure_pattern, $v, $matches);
            return $structurePrefixe . $matches['code'];
        }, $structuresRecherche);

        return in_array($value->getId(), array_merge($structures, $structuresRecherche));
    }

    /**
     * Retourne les étapes auxquelles appartient l'étudiant
     *
     * @return mixed|StructureEntity[]|null
     * @throws Exception
     */
    public function getInscriptionEtape()
    {
        $value = $this->preFormat($this->supannEtuEtape);
        if (empty($value)) return null;

        $label = $this->getLabel('UAI', $this->service->getLdapRootService()->getEtablissementUAI());
        $structureService = $this->service->getLdapStructureService();
        list($structureKey) = Entity::getNodeParams($structureService->getType());
        $structureBranch = $structureService->getBranches()[0];
        $value = array_map(function ($v) use ($label, $structureKey, $structureBranch) {
            return sprintf('%s=%s,%s',
                $structureKey,
                str_replace($label, '' , $v),
                $structureBranch
            );
        }, $value);

        $value = $structureService->getAllBy($value, 'dn');
        return 1 == count($value) ? array_shift($value) : $value;
    }

    /**
     * Retourne les éléments pédagogiques auxquels appartient l'étudiant
     *
     * @return mixed|StructureEntity[]|null
     * @throws Exception
     */
    public function getInscriptionElementPedagogique()
    {
        $value = $this->preFormat($this->supannEtuElementPedagogique);
        if (empty($value)) return null;

        $label = $this->getLabel('UAI', $this->service->getLdapRootService()->getEtablissementUAI());
        $structureService = $this->service->getLdapStructureService();
        list($structureKey) = Entity::getNodeParams($structureService->getType());
        $structureBranch = $structureService->getBranches()[0];
        $value = array_map(function ($v) use ($label, $structureKey, $structureBranch) {
            return sprintf('%s=%s,%s',
                $structureKey,
                str_replace($label, '' , $v),
                $structureBranch
            );
        }, $value);

        $value = $structureService->getAllBy($value, 'dn');
        return 1 == count($value) ? array_shift($value) : $value;
    }

    /**
     * Retourne les inscriptions complètes d'un étudiant
     *
     * @return array
     */
    public function getInscription()
    {
        $value = $this->preFormat($this->supannEtuInscription);
        if (empty($value)) return null;

        $value = array_map(function ($v) {
            preg_match(self::$inscription_pattern, $v, $matches);
            $affect = $matches['affect'];
            $etape = str_replace($this->getLabel('UAI', $this->service->getLdapRootService()->getEtablissementUAI()), '', $matches['etape']);
            $replace = [$this->getLabel('SISE'), $this->getLabel('SUPANN'), $this->getLabel('INCONNU')];
            return [
                'etab'          => str_replace($this->getLabel('UAI'), '', $matches['etab']),
                'anneeinsc'     => $matches['anneeinsc'],
                'regimeinsc'    => str_replace($replace, '', $matches['regimeinsc']),
                'sectdisc'      => str_replace($replace, '', $matches['sectdisc']),
                'typedip'       => str_replace($replace, '', $matches['typedip']),
                'cursusann'     => str_replace($replace, '', $matches['cursusann']),
                'affect'        => $this->service->getLdapStructureService()->get($affect) ?: $affect,
                'diplome'       => str_replace($replace, '', $matches['diplome']),
                'etape'         => $this->service->getLdapStructureService()->get($etape) ?: $etape,
            ];
        }, $value);

        return 1 == count($value) ? array_shift($value) : $value;
    }

    /**
     * Retourne le complément des inscriptions d'un étudiant
     *
     * @return array
     */
    public function getInscriptionComplement()
    {
        $value = $this->preFormat($this->ucbnEtuComplementInscription);
        if (empty($value)) return null;

        $value = array_map(function ($v) {
            preg_match(self::$inscription_complement_pattern, $v, $matches);
            $uaiLabel = $this->getLabel('UAI', $this->service->getLdapRootService()->getEtablissementUAI());
            $etape = str_replace($uaiLabel, '', $matches['etape']);
            return [
                'anneeinsc' => $matches['anneeinsc'],
                'etape'     => $this->service->getLdapStructureService()->get($etape) ?: $etape,
                'adistance' => str_replace($uaiLabel, '', $matches['adistance'])
            ];
        }, $value);

        return 1 == count($value) ? array_shift($value) : $value;
    }

    /**
     * Retourne les rôles d'une personne
     *
     * @return array
     */
    public function getRole()
    {
        $value = $this->preFormat($this->supannRoleEntite);
        if (empty($value)) return null;

        $value = array_map(function ($v) {
            preg_match(self::$role_pattern, $v, $matches);
            $code = $matches['code'];
            $replace = [$this->getLabel('SUPANN'), $this->getLabel('INCONNU')];
            return [
                'role'      => str_replace($replace, '', $matches['role']),
                'type'      => str_replace($replace, '', $matches['type']),
                'code'      => $this->service->getLdapStructureService()->get($code) ?: $code,
                'libelle'   => $matches['libelle'],
            ];
        }, $value);

        return 1 == count($value) ? array_shift($value) : $value;
    }

    /**
     * Vérifie si la personne à une fonction dans un type de structure et une structure
     *
     * @param string $role
     * @param string $type
     * @param string|Dn|StructureEntity|null $value
     * @return bool
     * @throws Exception
     * @throws \Laminas\Ldap\Exception\LdapException
     */
    public function hasRoleInStructure(string $role, string $type, $structure = null): bool
    {
        $structureService = $this->service->getLdapStructureService();
        $structurePrefixe = $structureService->getCodeStructurePrefixe();

        if($structure == null) {
            $structure = '.*';
        }
        else {
            if (is_string($structure)) {
                if (Dn::checkDn($structure)) {
                    $structure = $structureService->getBy($structure, 'dn');
                } else {
                    if (0 !== strpos($structure, $structurePrefixe)) {
                        $structure = $structurePrefixe . $structure;
                    }
                    $structure = $structureService->get($structure);
                }
            } elseif ($structure instanceof Dn) {
                $structure = $structureService->getBy($structure->toString(), 'dn');
            }

            if (!$structure instanceof StructureEntity) {
                return false;
            }
        }

        $structure = $structure instanceof StructureEntity ? $structure->supannCodeEntite : $structure;
        $pattern = "/^\[role=\{SUPANN\}$role\]\[type=\{SUPANN\}$type\]\[code=$structure\]\[libelle=.*\]$/";

        $matches = array_filter($this->supannRoleEntite, function ($v) use ($pattern) {
            return preg_match($pattern, $v);
        });

        return !empty($matches);
    }

    /**
     * Retourne le site d'une personne
     *
     * @return array|null
     */
    public function getSite()
    {
        $value = $this->preFormat($this->ucbnSiteLocalisation);
        if (empty($value)) return null;

        $value = array_map(function ($v) {
            preg_match(self::$localisation_pattern, $v, $matches);
            $code = $matches['code'];
            return [
                'code'      => $matches['code'],
                'libelle'   => $matches['libelle'],
            ];
        }, $value);

        return 1 == count($value) ? array_shift($value) : $value;
    }

    /**
     * Retourne la liste des groupes dont l'utilisateur fait partie
     * Si le groupe n'est plus valide à la date d'observation, alors il n'est pas retourné dans la liste
     *
     * @param DateTime $dateObservation
     * @param string   $orderBy Champ de tri (au besoin)
     * @return GroupEntity[]|null
     */
    public function getGroups(DateTime $dateObservation = null, $orderBy = null)
    {
        $groupService = $this->service->getLdapGroupService();

        return $groupService->filterValids($groupService->getAllBy($this->get('memberOf'), 'dn', $orderBy), $dateObservation);
    }

    /**
     * Modifie l'ensemble des attributs liés au mot de passe
     *
     * @param string $value
     * @return $this
     * @throws \UnicaenLdap\Exception
     * @throws \Laminas\Ldap\Exception\LdapException
     */
    public function setPassword(string $value)
    {
        parent::setUserPassword($value);
        parent::setNtPassword($value);
        parent::setSambaNTPassword($value);
        parent::setCampusPassword($value);

        return $this;
    }
}