<?php

namespace UnicaenLdap\Entity;

use UnicaenLdap\Entity\Base\Structure as BaseStructure;
use UnicaenLdap\Filter\Filter;
use Laminas\Ldap\Exception\LdapException;

/**
 * Classe de gestion des entités de la branche "structures" de l'annuaire LDAP.
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 * @author David SURVILLE <david.surville@unicaen.fr>
 */
class Structure extends BaseStructure
{
    /**
     * Retourne la structure parente, si elle existe
     *
     * @return Structure[]
     */
    public function getParents()
    {
        if (null !== ($parentIds = $this->get('supannCodeEntiteParent'))) {
            return $this->service->getAll($parentIds);
        }

        return null;
    }

    /**
     * Retourne la liste des structures filles
     *
     * @param string|Filter $filter  Filtre éventuel
     * @param string        $orderBy Champ de tri
     * @return Structure[]
     */
    public function getChildren($filter = null, $orderBy = null)
    {
        $childrenFilter = Filter::equals('supannCodeEntiteParent', $this->getId());

        if (empty($filter)) {
            $filter = $childrenFilter;
        } else {
            if (is_string($filter)) $filter = Filter::string($filter);
            $filter = Filter::andFilter($childrenFilter, $filter);
        }

        return $this->service->search($filter, $orderBy, -1, 0);
    }

    /**
     * Retourne le code dans la base source
     *
     * @return string
     * @throws LdapException
     * @throws \UnicaenLdap\Exception
     */
    public function getCodeSource()
    {
        $code = $this->getId();
        return (
            0 === strpos($code, $this->service->getCodeStructurePrefixe()) ||
            0 === strpos($code, $this->service->getCodeModuleEnseignementPrefixe()))
            ? substr($code, 3)
            : $code;
    }
}