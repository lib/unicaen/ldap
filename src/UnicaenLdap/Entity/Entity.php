<?php

namespace UnicaenLdap\Entity;

use UnicaenLdap\Node;
use UnicaenLdap\Exception;
use UnicaenLdap\Service\AbstractService;
use UnicaenLdap\Util;
use Laminas\Ldap\Dn;
use Laminas\Ldap\Exception\LdapException;

/**
 * Classe mère des entrées de l'annuaire LDAP.
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
abstract class Entity
{
    /**
     * Type d'entité
     *
     * @var string
     */
    protected $type;

    /**
     * Service qui gère l'entité
     *
     * @var AbstractService
     */
    protected $service;

    /**
     * @var Node
     */
    protected $node;

    /**
     * Liste des classes d'objet nécessaires à la création de l'entité
     *
     * @var array
     */
    protected $objectClass = [];

    /**
     * Liste des attributs autorisés pour une entité
     *
     * @var array
     */
    protected $authorizedAttributes = [];

    /**
     * Liste des attributs contenant des dates communs à toutes les entités
     */
    private $sharedDateTimeAttributes = [
        'createTimestamp',
        'modifyTimestamp',
    ];

    /**
     * Liste des attributs contenant des dates
     *
     * @var array
     */
    protected $dateTimeAttributes = [];

    /**
     * Liste des attributs monovalués
     *
     * @var array
     */
    protected $monoValuedAttributes = [];

    /**
     * Liste des patterns génériques utilisés pour différents attributs
     */
    static protected $attribute_with_label_pattern = '/^(?<etiquette>\{[\w\-:]+\})(?<identifiant>.+)$/';

    /**
     * Liste des patterns spécifiques utilisés pour différents attributs
     */
    static protected $postal_address_pattern = '/^(.*)\$(.*)\$(.*)\$(.*)\$(.*)\$(.*)$/';


    /**
     * @param string $type type d'entité
     * @return array
     * @throws Exception
     */
    public static function getNodeParams($type)
    {
        $params = [
            'Generic' => ['uid', 'UnicaenLdap\\Entity\\Generic'],
            'Group' => ['cn', 'UnicaenLdap\\Entity\\Group'],
            'People' => ['uid', 'UnicaenLdap\\Entity\\People'],
            'Root' => ['dc', 'UnicaenLdap\\Entity\\Root'],
            'Structure' => ['supannCodeEntite', 'UnicaenLdap\\Entity\\Structure'],
            'System' => ['uid', 'UnicaenLdap\\Entity\\System'],
        ];
        if (!isset($params[$type])) throw new Exception('Paramètres de "' . $type . '" non trouvés');

        return $params[$type];
    }


    /**
     * Vérifie le format de l'identifiant Ldap selon le type de l'entité
     *
     * @param $type
     * @param $id
     * @return false|int
     */
    public static function checkIdFormat($type, $id)
    {
        switch($type) {
            default:
                return preg_match('/^[[:alnum:]_-]+$/', $id);
        }
    }

    /**
     * Entity constructor.
     *
     * @param AbstractService $service Service qui gèrera la future entité
     * @param Node|Dn|array|string $data Noeud si Node, sinon DN
     * @throws LdapException
     */
    public function __construct(AbstractService $service, $data)
    {
        $this->service = $service;

        if ($data instanceof Node) {
            $this->setNode($data);
        } else {
            $this->setNode(Node::create($data, $this->objectClass));
        }
    }

    /**
     * Retourne le type de l'entité
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Retourne le service qui gère l'entité
     *
     * @return AbstractService
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Retourne le nœud Ldap de base
     *
     * @return Node
     */
    public function getNode()
    {
        return $this->node;
    }

    /**
     * Affecte un nœud de base
     *
     * @param Node $node
     */
    public function setNode(Node $node)
    {
        $this->node = $node;
    }

    /**
     * Retourne le Dn de l'entité sous forme de chaîne de caractères
     *
     * @return string
     */
    public function getDn()
    {
        return $this->getNode()->getDnString();
    }

    /**
     * Retourne l'Organizational Unit (OU) de l'entité
     *
     * @return string
     */
    public function getOu()
    {
        if ($result = $this->getNode()->getDn()->get(1)) {
            return $result['ou'];
        }

        return null;
    }

    /**
     * Retourne le nom de la clé primaire correspondant à l'entité
     *
     * @return mixed
     * @throws Exception
     */
    public function getKey()
    {
        $params = self::getNodeParams($this->type);
        return reset($params);
    }

    /**
     * Retourne la valeur de la clé primaire correspondant à l'entité
     *
     * @return string
     * @throws Exception
     * @throws LdapException
     */
    public function getId()
    {
        return $this->get($this->getKey());
    }

    /**
     * Retourne l'attribut "objectClass"
     *
     * @return array|int|mixed|null
     * @throws LdapException
     */
    public function getObjectClass()
    {
        return $this->get('objectClass');
    }

    /**
     * Retourne la liste des attributs de l'entité
     *
     * @param  bool $includeSystemAttributes
     * @return string[]
     */
    public function getAttributesList($includeSystemAttributes = true)
    {
        return array_keys($this->getNode()->getAttributes($includeSystemAttributes));
    }

    /**
     * Retourne un label constitué des différentes clés passées en paramètre
     * Format : {$key1:$key2:$key3:...}
     *
     * @param string ...$key
     * @return string
     */
    public function getLabel(...$key)
    {
        $label = '';
        $c = 0;
        foreach($key as $k) {
            if(is_string($k)) {
                $label .= ($c == 0) ? $k : ":$k";
            }
            $c++;
        }

        return sprintf('{%s}', $label);
    }

    /**
     * Exporte sous forme de tableau le contenu de l'entité
     *
     * @return array
     * @throws LdapException
     */
    public function toArray()
    {
        $result = [];
        $attrsList = $this->getAttributesList();
        foreach ($attrsList as $attrName) {
            $result[$attrName] = $this->get($attrName);
        }

        return $result;
    }

    /**
     * Retourne un attribut
     *
     * @param string $attrName
     * @return array|int|mixed|null
     * @throws LdapException
     */
    public function get($attrName)
    {
        if (in_array($attrName, array_merge($this->dateTimeAttributes, $this->sharedDateTimeAttributes))) {
            $value = $this->getNode()->getDateTimeAttribute($attrName);
        } else {
            $value = $this->getNode()->getAttribute($attrName);
        }
        if (empty($value)) {
            $value = null;
        } elseif (1 == count($value)) {
            $value = $value[0];
        }

        return $value;
    }

    /**
     * Affecte une nouvelle valeur à un attribut
     *
     * This is an offline method.
     * Node will be updated on calling update().
     *
     * @param string $attrName
     * @param mixed $value
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function set($attrName, $value)
    {
        if(!in_array($attrName, $this->authorizedAttributes)) {
            throw new Exception(sprintf("L'attribut Ldap '%s' n'est pas autorisé pour une entité '%s'.", $attrName, get_class($this)));
        }

        if(is_array($value)
            && count($value) > 1
            && in_array($attrName, $this->monoValuedAttributes)) {
            throw new Exception(sprintf("L'attribut Ldap '%s' est monovalué et ne doit contenir qu'une seule valeur.", $attrName));
        }

        if (in_array($attrName, array_merge($this->dateTimeAttributes, $this->sharedDateTimeAttributes))) {
            $this->getNode()->setDateTimeAttribute($attrName, $value, true);
        } else {
            $this->getNode()->setAttribute($attrName, $value);
        }

        return $this;
    }

    /**
     * Retourne <code>true</code> si l'attribut $param possède la valeur $value, <code>false</code> sinon.
     *
     * @param string $attrName
     * @param mixed $value
     * @return boolean
     */
    public function has($attrName, $value)
    {
        return $this->getNode()->attributeHasValue($attrName, $value);
    }

    /**
     * Ajoute une valeur à un attribut
     * Si l'attribut est monovalué, la valeur actuelle est remplacée par la nouvelle valeur
     *
     * This is an offline method.
     * Node will be updated on calling update().
     *
     * @param string $attrName
     * @param mixed $value
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function add($attrName, $value)
    {
        if(!in_array($attrName, $this->authorizedAttributes)) {
            throw new Exception(sprintf("L'attribut Ldap '%s' n'est pas autorisé pour une entité '%s'.", $attrName, get_class($this)));
        }

        if(in_array($attrName, $this->monoValuedAttributes)) {
            $this->set($attrName, $value);
        }
        else {
            if (in_array($attrName, array_merge($this->dateTimeAttributes, $this->sharedDateTimeAttributes))) {
                $this->getNode()->appendToDateTimeAttribute($attrName, $value);
            } else {
                $this->getNode()->appendToAttribute($attrName, $value);
            }

            return $this;
        }
    }

    /**
     * Retire une valeur à un attribut
     *
     * This is an offline method.
     * Node will be updated on calling update().
     *
     * @param string $attrName
     * @param mixed|array $value
     * @return self
     */
    public function remove($attrName, $value)
    {
        $this->getNode()->removeFromAttribute($attrName, $value);

        return $this;
    }

    /**
     * Ajoute ou affecte une valeur à un attribut Ldap
     *
     * This is an offline method.
     * Node will be updated on calling update().
     *
     * @param string $attrName
     * @param mixed $value
     * @param bool $append
     * @throws Exception
     * @throws LdapException
     */
    protected function appendOrNot($attrName, $value, $append)
    {
        (!$append)
            ? $this->set($attrName, $value)
            : $this->add($attrName, $value);
    }

    /**
     * Méthode magique...
     *
     * @param string $attrName
     * @return mixed
     * @throws LdapException
     */
    public function __get($attrName)
    {
        return $this->get($attrName);
    }

    /**
     * Méthode magique...
     *
     * @param string $attrName
     * @param mixed $value
     * @return self
     * @throws LdapException
     */
    public function __set($attrName, $value)
    {
        return $this->set($attrName, $value);
    }

    /**
     * Methode magique ...
     *
     * @param string $method Nom de la méthode à appeler
     * @param array $arguments Tableau de paramètres
     * @return mixed
     */
    public function __call($method, array $arguments)
    {
        $methods = ['get', 'set', 'has', 'add', 'remove'];
        foreach ($methods as $methodName) {
            if (0 === strpos($method, $methodName)) {
                $attrName = lcfirst(substr($method, strlen($methodName)));
                $arguments = array_merge((array)$attrName, $arguments);

                return call_user_func_array([$this, $methodName], $arguments);
            }
        }
    }

    /**
     * Teste si l'entité existe
     *
     * @return bool
     * @throws LdapException
     */
    public function exists()
    {
        return $this->attach()->getNode()->exists();
    }

    /**
     * Déplacement de l'entité dans un autre OU (Organizational Unit)
     *
     * This is an offline method.
     * Node will be moved on calling update().
     *
     * @param string | Dn $newDn
     * @throws \Laminas\Ldap\Exception\LdapException
     */
    public function move(string $newOu)
    {
        $arrayOu = $this->getService()->getOu();

        if(empty($arrayOu) || !in_array($newOu, $arrayOu)) {
            throw new Exception(sprintf("L'entité ne peut être déplacée dans l'OU '%s'.", $newOu));
        }

        if($newOu == $this->getOu()) {
            throw new Exception(sprintf("L'entité est déjà présente dans l'OU '%s'.", $newOu));
        }

        $newDn = sprintf('%s=%s,ou=%s,%s', $this->getKey(), $this->getId(), $newOu, $this->getNode()->getLdap()->getBaseDn());

        $this->getNode()->move($newDn);

        return $this;
    }

    /**
     * Renommage de l'entité
     *
     * This is an offline method. Node will be renamed on calling update().
     *
     * @param string | Dn $newDn
     * @throws \Laminas\Ldap\Exception\LdapException
     */
    public function rename($newId)
    {
        $newDn = sprintf('%s=%s,ou=%s,%s', $this->getKey(), $newId, $this->getOu(), $this->getNode()->getLdap()->getBaseDn());

        $this->getNode()->move($newDn);
    }

    /**
     * Suppression de l'entité
     *
     * This is an offline method.
     * Node will be deleted on calling update().
     *
     * @return self
     */
    public function delete()
    {
        $this->getNode()->delete();

        return $this;
    }

    /**
     * Mise à jour de l'entité
     *
     * @return self
     * @throws LdapException
     */
    public function update()
    {
        $this->getNode()->update();

        return $this;
    }

    /**
     * Attache une connexion
     *
     * @return self
     * @throws LdapException
     */
    public function attach()
    {
        $this->getNode()->attachLdap($this->service->getLdap());

        return $this;
    }

    /**
     * Formate un nom ou un prénom
     *
     * @param string $name
     * @param bool $removeAccents remplace les caractères avec un signe diacritique
     * @return mixed|null|string|string[]
     */
    protected function formatName($name, $removeAccents = false)
    {
        $name = preg_replace('/[[:blank:]]+/', ' ', trim($name));

        if ($removeAccents) {
            $name = Util::removeAccents($name);
        }

        return mb_convert_case($name, MB_CASE_TITLE, "UTF-8");
    }

    /**
     * Formate un numéro de téléphone (0XXXXXXXXX) au format international (+33 X XX XX XX XX)
     *
     * @param string $num
     * @return string
     */
    protected function formatTel($num)
    {
        if (is_null($num)) {
            return null;
        }

        $num = preg_replace('/[\.\-\s]+/', '', $num);

        return preg_match("/0\d{9}/", $num)
            ? preg_replace("/0(\d{1})(\d{2})(\d{2})(\d{2})(\d{2})/", "+33 $1 $2 $3 $4 $5", $num)
            : $num;
    }

    /**
     * Formate les données sources correspondant à un booléen
     *
     * @param mixed $value
     * @return string
     */
    protected function formatBoolean($value)
    {
        if (!is_bool($value) && $value === null) {
            return null;
        }

        if (is_string($value)) {
            $value = strtolower($value);
        }

        switch (true) {
            case $value === false;
            case $value === 0:
            case $value === 'n':
                return 'FALSE';
            case $value === true:
            case $value === 1:
            case $value === 'o':
            case $value === 'y':
                return 'TRUE';
            default:
                return null;
        }
    }

    /**
     * Vérifie une valeur et supprime les valeurs nulles
     *
     * @param mixed $value
     * @return array
     */
    protected function preFormat($value)
    {
        if (is_scalar($value)
            || is_null($value)
            || is_a($value, 'DateTime')) {
            $value = [$value];
        }

        $value = array_filter($value, 'strlen'); // Delete NULL, '' and FALSE values

        return $value;
    }
}