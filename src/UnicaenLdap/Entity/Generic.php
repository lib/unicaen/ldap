<?php

namespace UnicaenLdap\Entity;

use UnicaenLdap\Entity\Base\Generic as BaseGeneric;

/**
 * Classe de gestion des entités de la branche "generic" de l'annuaire LDAP.
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class Generic extends BaseGeneric
{}