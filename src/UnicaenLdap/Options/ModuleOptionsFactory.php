<?php

namespace UnicaenLdap\Options;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 * Description of ModuleOptionsFactory
 *
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */
class ModuleOptionsFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config       = $container->get('Configuration');
        $moduleConfig = isset($config['unicaen-ldap']) ? $config['unicaen-ldap'] : array();

        return new ModuleOptions($moduleConfig);
    }
}