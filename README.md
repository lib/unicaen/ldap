# UnicaenLdap

 * [Introduction](#introduction)
 * [Installation](#installation)
 * [Configuration](#configuration)
 
## Introduction
 
Ce module permet de se connecter à l'annuaire Ldap de l'université et de consulter/modifier ses données. 

## Pré-requis

L'utilisation de ce module nécessite l'installation de l'extension `ext-ldap` de PHP.

## Installation

```bash 
$ composer require unicaen/ldap
```

## Configuration

> Récupérer les fichiers de config du module 
 
```bash
$ cp -n vendor/unicaen/zimbra/config/unicaen-ldap.local.php.dist config/autoload/unicaen-ldap.local.php
```

> Adapter le contenu à vos besoins en configurant notamment les paramètres de connexion au serveur Ldap.

```php
'unicaen-ldap' => [
    'host'                => 'ldap-test.unicaen.fr',
    'port'                => 389,
    'version'             => 3,
    'baseDn'              => "dc=unicaen,dc=fr", // racine de l'annuaire
    'bindRequiresDn'      => true,
    'username'            => "uid=xxxx,ou=system,dc=unicaen,dc=fr",
    'password'            => "xxxx",
    'accountFilterFormat' => "(&(objectClass=supannPerson)(supannAliasLogin=%s))",
    'useStartTls'         => false
]
```